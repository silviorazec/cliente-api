package br.com.platform.builders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class ClientesApiApplicationTests {
	
	@Autowired
	private MockMvc mockMvc;


	@Test
	void pesquisClientesPorParametros() throws Exception {
		this.mockMvc.perform(get("/v1/clientes/?pagina=1&nome=a&cpf=0")).andDo(print()).andExpect(status().isOk());
	}
	
	@Test
	void pesquisClientePorCPF() throws Exception {
		this.mockMvc.perform(get("/v1/clientes/54986102036")).andDo(print()).andExpect(status().isOk());
	}
	
	@Test
	void pesquisClientePorCPFNotFoud() throws Exception {
		this.mockMvc.perform(get("/v1/clientes/30330856014")).andDo(print()).andExpect(status().isNotFound());
	}
	


}
