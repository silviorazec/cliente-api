package br.com.platform.builders.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import br.com.platform.builders.utils.DataValidator;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = DataValidator.class)
@Documented
public @interface Data {

    String message() default "Não é uma data válida";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
    
    String pattern();

}