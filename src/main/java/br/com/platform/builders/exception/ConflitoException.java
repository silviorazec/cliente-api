package br.com.platform.builders.exception;

public class ConflitoException extends Throwable {

	private static final long serialVersionUID = 1L;
	
	public ConflitoException(String mensagem) {
		super(mensagem);
	}

}
