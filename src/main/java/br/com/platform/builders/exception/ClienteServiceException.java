package br.com.platform.builders.exception;

public class ClienteServiceException extends Exception{


	private static final long serialVersionUID = 1L;

	public ClienteServiceException(Exception e) {
		super(e);
	}
}
