package br.com.platform.builders.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.platform.builders.exception.ClienteServiceException;
import br.com.platform.builders.exception.ConflitoException;
import br.com.platform.builders.modelo.Cliente;
import br.com.platform.builders.modelo.Erro;
import br.com.platform.builders.service.ClienteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping({"v1/clientes/"})
@Tag(description = "Serviços do Objeto Cliente", name = "Cliente")
public class ClienteController extends BaseController{
	
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private HttpServletRequest request;
	
	@Operation(description = "Recuperar lista paginada de clientes a partir de parâmetros", method = "GET")
	@ApiResponse(responseCode = "200", description = "Sucesso", 
					content = @Content(array = @ArraySchema(schema = @Schema(implementation = Cliente.class))),
					headers = {@Header(name = "anterior",description = "link para o recurso anterior da paginação"),
							   @Header(name = "proximo",description = "link para o próximo recurso da paginação"),
							   @Header(name = "totalRegistros",description = "Total de registros que a consulta retorna")})

	@ApiResponse(responseCode = "500", description = "Erro Interno do Serviro", content = @Content(schema = @Schema(implementation = Erro.class)))
	@ApiResponse(responseCode = "404", description = "Nada encontrado")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> pesquisarClientes(
													@Parameter(name = "pagina", required = true, example = "1", description = "Página atual da pesquisa", in = ParameterIn.QUERY)
													@RequestParam(name = "pagina",required = true) Integer pagina,
													@Parameter(name = "nome", example = "tobia", description = "Nome ou parte do nome do cliente",in = ParameterIn.QUERY)
													@RequestParam(name="nome", required = false) String nome,
													@Parameter(name = "cpf", example = "097", description = "CPF ou parte do cpf do cliente",in = ParameterIn.QUERY)
													@RequestParam(name="cpf",required = false) String cpf ) {
		
		try {
			return getOkOuNotFoundPaginado(clienteService.pesquisar(nome, cpf, pagina),request);
		}catch (Exception e) {
			return getErroInternoServidorResponse();
		}
	}
	
	
	
	@Operation(description = "Recuperar um cliente a partir do seu código", method = "GET")
	@ApiResponse(responseCode = "200", description = "Sucesso", 
					content = @Content(schema = @Schema(implementation = Cliente.class)))
	@ApiResponse(responseCode = "500", description = "Erro Interno do Serviro", content = @Content(schema = @Schema(implementation = Erro.class)))
	@ApiResponse(responseCode = "404", description = "Nada encontrado")
	@GetMapping(path = {"{cpf}"},produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getClientePorCPF(
													@Parameter(required = true,name = "cpf", description = "CPF completo, somente números, do cliente",in = ParameterIn.PATH)
													@PathVariable String cpf ) {
		try {
			return getOkOuNotFound(clienteService.getByCPF(cpf));
		}catch (ConflitoException e) {
			return getConflictResponse(e);
		}catch (ClienteServiceException e) {
			return getErroInternoServidorResponse();
		}
	}
	
	@Operation(description = "Realizar PUT (atualizar todas as informações ou cadastrar novo registro) em um cliente a partir do código", method = "PUT")
	@ApiResponse(responseCode = "201", description = "Recurso criado",
					headers =@Header(name = "location",description = "Em caso de novo registro, o link informarar a uri do recurso"))
	@ApiResponse(responseCode = "202", description = "Sucesso")
	@ApiResponse(responseCode = "500", description = "Erro Interno do Serviro", content = @Content(schema = @Schema(implementation = Erro.class)))
	@PutMapping(path = {"{cpf}"},consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> putCliente(@Valid
											@RequestBody
											Cliente cliente, 
											@Parameter(name = "cpf", description = "CPF completo, somente números, do cliente",in = ParameterIn.PATH)
											@PathVariable String cpf){
		try {
			HttpStatus httpStatus = clienteService.putCliente(cliente,cpf);
			return getCreatedOrOk(httpStatus, cliente.getCpf(), request);
		}catch (ConflitoException e) {
			return getConflictResponse(e);
		}catch (Exception e) {
			return getErroInternoServidorResponse();
		}
	}
	
	@Operation(description = "Atualizar dados do Cliente a partir do código", method = "PATCH")
	@ApiResponse(responseCode = "202", description = "Sucesso")
	@ApiResponse(responseCode = "500", description = "Erro Interno do Serviro", content = @Content(schema = @Schema(implementation = Erro.class)))
	@PatchMapping(path = {"{cpf}"},consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> patchCliente(

												@RequestBody HashMap<String, Object> mapCliente,
												@Parameter(name = "cpf", description = "CPF completo, somente números, do cliente",in = ParameterIn.PATH)
												@PathVariable(name = "cpf",required = true) String cpf){
		try {
			clienteService.atualizarCliente(mapCliente, cpf);
			return getResponseOK();
	
		} catch (ConflitoException e) {
			return getConflictResponse(e);
		}catch (Exception e) {
			return getErroInternoServidorResponse();
		}
	}
	
	@Operation(description = "Cadastra um novo cliente", method = "POST")
	@ApiResponse(responseCode = "201", description = "Recurso criado",
					headers =@Header(name = "location",description = "Link para o do novo recurso"))
	@ApiResponse(responseCode = "500", description = "Erro Interno do Serviro", content = @Content(schema = @Schema(implementation = Erro.class)))
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> cadastrarCliente(@Valid @RequestBody Cliente cliente){
		try {
			clienteService.salvarCliente(cliente);
			return getCreated(cliente.getCpf(), request);
		}catch (ConflitoException e) {
			return getConflictResponse(e);
		}catch (Exception e) {
			return getErroInternoServidorResponse();
		}
	
	}
	
	@Operation(description = "Apaga um cliente a partir do código", method = "DELETE")
	@ApiResponse(responseCode = "204", description = "Sem conteúdo")
	@ApiResponse(responseCode = "500", description = "Erro Interno do Serviro", content = @Content(schema = @Schema(implementation = Erro.class)))
	@DeleteMapping(path = {"{cpf}"})
	public ResponseEntity<Object> apagarCliente(
												@Parameter(name = "cpf", description = "CPF completo, somente números, do cliente",in = ParameterIn.PATH)
												@PathVariable String cpf 
												) {
		try {
			clienteService.apagar(cpf);
			return getResponseNotContent();
		}catch (ConflitoException e) {
			return getConflictResponse(e);
		}catch (ClienteServiceException e) {
			return getErroInternoServidorResponse();
		}
	}

}
