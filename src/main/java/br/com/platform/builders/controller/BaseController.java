package br.com.platform.builders.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.platform.builders.modelo.Erro;

public class BaseController {
	
	private static final String ERRO_INTERNO_SERVIDOR_MSG = "Ocorreu um erro no servidor do serviço";
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
	    Map<String, Object> errors = new HashMap<>();
	    List<String> listaErros = new ArrayList<String>();
	    errors.put("codigo", HttpStatus.BAD_REQUEST.value());
	    errors.put("descricao", HttpStatus.BAD_REQUEST.toString());
	   
	    ex.getBindingResult().getFieldErrors().forEach(error -> 
	        listaErros.add(error.getField() + ": " + error.getDefaultMessage())
	    );
	    
	    errors.put("erros", listaErros);
	    return errors;
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<Object> handleMyException(Exception  exception) {
     return getGenricoResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
    } 

	public  ResponseEntity<Object> getOkOuNotFound(Object obj) {
		
		if(obj != null) {
			return ResponseEntity.status(HttpStatus.OK).body(obj);
					
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	public ResponseEntity<Object> getResponseOK(){
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	public ResponseEntity<Object> getResponseNotContent(){
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}
	
	public ResponseEntity<Object> getCreated(Object identificador,HttpServletRequest request) throws URISyntaxException{
		String urlNovoRecurso = request.getRequestURI() + identificador;
		return ResponseEntity.created(new URI(urlNovoRecurso)).build();
	}
	
	public  ResponseEntity<Object> getOkOuNotFoundPaginado(Page page, HttpServletRequest request) {

		if(page != null && !page.getContent().isEmpty()) {
			HttpHeaders responseHeaders = new HttpHeaders();
			
			if(!page.isLast()) {
				responseHeaders.set("proximo", getUrlComParametros(request,(page.getNumber() + 2)));
			}
			
			if(!page.isFirst()) {
				responseHeaders.set("anterior", getUrlComParametros(request, page.getNumber()) );
			}
			
			responseHeaders.set("totalRegistros",String.valueOf(page.getTotalElements()));
			
			return ResponseEntity.status(HttpStatus.OK).headers(responseHeaders).body(page.getContent());
					
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	public  ResponseEntity<Object> getErroInternoServidorResponse(){
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(getObjetoErro(HttpStatus.INTERNAL_SERVER_ERROR, ERRO_INTERNO_SERVIDOR_MSG));
	}
	
	public  ResponseEntity<Object> getGenricoResponse(HttpStatus httpStatus, String mensagem){
		return ResponseEntity.status(httpStatus).body(getObjetoErro(httpStatus, mensagem));
	}
	
	public ResponseEntity<Object> getConflictResponse(Throwable e){
		return getGenricoResponse(HttpStatus.CONFLICT, e.getMessage());
	}
	
	public ResponseEntity<Object> getCreatedOrOk(HttpStatus httpStatus, Object identificador, HttpServletRequest request) throws URISyntaxException{
		if(httpStatus.equals(HttpStatus.CREATED)) {
			return getCreated(identificador, request);
		}else {
			return ResponseEntity.ok().build();
		}
	}

	
	private  Erro getObjetoErro(HttpStatus httpStatus, String mensagem) {
		Erro erro = new Erro();
		
		erro.setCodigo(httpStatus.value());
		erro.setDescricao(httpStatus.toString());
		erro.setMensagemErro(mensagem);
		
		return erro;
		
	}
	
	private  String getUrlComParametros(HttpServletRequest request, int novaPagina) {
		
		StringBuilder  builder = new StringBuilder(request.getRequestURI());
		for( Entry<String, String[]> p : request.getParameterMap().entrySet()) {
			builder.append(p.getKey());
			builder.append("=");
			if(p.getKey().equalsIgnoreCase("pagina")) {
				builder.append(novaPagina);
			}else {
				builder.append(arrayStringToString(p.getValue()));
			}
			builder.append("&");
		}
		
		return builder.substring(0,builder.lastIndexOf("&"));
	}
	
	private  String arrayStringToString(String[] valores) {
		StringBuilder builder = new StringBuilder();
		for(String valor : valores) {
			builder.append(valor);
			builder.append(",");
		}
		return builder.substring(0,builder.lastIndexOf(","));
	}
}
