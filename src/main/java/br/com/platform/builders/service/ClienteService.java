package br.com.platform.builders.service;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.platform.builders.exception.ClienteServiceException;
import br.com.platform.builders.exception.ConflitoException;
import br.com.platform.builders.modelo.Cliente;
import br.com.platform.builders.repositories.ClienteRepository;
import br.com.platform.builders.utils.UtilsValidate;

@Service
public class ClienteService extends BaseService {
	
	private static final int TOTAL_REGISTROS_POR_PAGINA = 5;
	private static final String CLIENTE_INEXISTENTE = "Não existe um cliente cmo o cpf informado";
	private static final String CLIENTE_EXISTENTE = "Já existe um cliente registrado com o CPF informado";
	private static final String CLIENTE_EXISTENTE_DIFERENTE_INFORMADO = "Há um conflito de informações no CPF. Só é permitida realizar 'PUT' "
																		+" caso os CPF's informados correspondam. Ou, caso não correspondam, se o "
																		+ " cpf registrado no json já não estiver em uso, na base, e o cpf informado "
																		+ " na queryString exista.";

	private static final String CPF_INVALIDO = "CPF inválido";
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	public Cliente getByCPF(String cpf) throws ConflitoException, ClienteServiceException {
		try {
			if(UtilsValidate.isCPF(cpf)) {
				return clienteRepository.findByCpf(cpf);
			}else {
				throw new ConflitoException(CPF_INVALIDO);
			}
		}catch (Exception e) {
			throw registrarErro(e);
		}
		
	}
	
	public Page<Cliente> pesquisar(String nome, String cpf, Integer pagina) throws ClienteServiceException{
		
		try {
			Pageable pageable = PageRequest.of(pagina - 1, TOTAL_REGISTROS_POR_PAGINA);
			
			return clienteRepository.findAllByNomeContainingIgnoreCaseAndCpfContaining(UtilsValidate.nullToEmptyString(nome),
																						UtilsValidate.nullToEmptyString(cpf),
																						pageable);
		}catch (Exception e) {
			throw registrarErro(e);
		}
	}
	
	public void salvarCliente(Cliente cliente) throws ConflitoException, ClienteServiceException {
		
		try {
			Cliente clienteExistente = clienteRepository.findByCpf(cliente.getCpf());
			
			if(clienteExistente == null) {
				clienteRepository.save(cliente);
			}else {
				throw new ConflitoException(CLIENTE_EXISTENTE);
			}
		}catch (Exception e) {
			throw registrarErro(e);
		}
	}
	
	public HttpStatus putCliente(Cliente cliente, String cpfConsultado) throws ClienteServiceException, ConflitoException {
		try {
			Cliente clienteExistenteJson = clienteRepository.findByCpf(cliente.getCpf());
			Cliente clienteExistenteQueryString = clienteRepository.findByCpf(cpfConsultado);
			HttpStatus status = null;
			
	
			if(isPutUpdateAutorizado(clienteExistenteJson, clienteExistenteQueryString)) {
				cliente.setId(clienteExistenteQueryString.getId());
				status = HttpStatus.OK;
			}else if(isPutCreateAutorizado(clienteExistenteJson, clienteExistenteQueryString, cliente.getCpf(), cpfConsultado)){
				 status = HttpStatus.CREATED;
			}else {
				throw new ConflitoException(CLIENTE_EXISTENTE_DIFERENTE_INFORMADO);
			}
	
			clienteRepository.save(cliente);
			
			return status;
		}catch (Exception e) {
			throw registrarErro(e);
		}
	}
	
	public void atualizarCliente(HashMap<String, Object> mapCliente, String cpfConsultado) throws ClienteServiceException, ConflitoException {
		try {
			Cliente cliente = getByCPF(cpfConsultado);
			
			if(cliente != null) {
				Class<Cliente> classe = Cliente.class;
				Field f = null;
				Object valor = null;
				for(Entry<String, Object> m : mapCliente.entrySet()) {
					f = classe.getDeclaredField(m.getKey());
					f.setAccessible(true);
					if(f.getAnnotation(JsonFormat.class) != null) {
						valor = new SimpleDateFormat(f.getAnnotation(JsonFormat.class).pattern()).parse(m.getValue().toString());
					}else {
						valor = m.getValue();
					}
					f.set(cliente, valor);
				}
				clienteRepository.save(cliente);
			}else {
				throw new ConflitoException(CLIENTE_INEXISTENTE);
			}

		}catch (Exception e) {
			throw registrarErro(e);
		}
	}
	
	public void apagar(String cpf) throws ClienteServiceException, ConflitoException {
		try {
			Cliente cliente = clienteRepository.findByCpf(cpf);
			if(cliente != null) {
				clienteRepository.delete(cliente);
			}
		}catch (Exception e) {
			throw registrarErro(e);
		}
		
	}
	
	private boolean isPutUpdateAutorizado(Cliente Clientejson, Cliente ClienteQueryString) {
		return (Clientejson != null && ClienteQueryString != null && Clientejson.getCpf().equals(ClienteQueryString.getCpf())) ||
				(Clientejson == null && ClienteQueryString !=null);	
	}

	private boolean isPutCreateAutorizado(Cliente Clientejson, Cliente ClientequeryString, String cpfJson, String cpfQueryString) {
		return (Clientejson == null && ClientequeryString == null) && cpfJson.equals(cpfQueryString);	
	}

	@Override
	@PostConstruct
	public void createLogger() {
		log = Logger.getLogger(ClienteService.class);
		
	}

}
