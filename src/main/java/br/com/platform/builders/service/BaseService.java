package br.com.platform.builders.service;

import org.jboss.logging.Logger;

import br.com.platform.builders.exception.ClienteServiceException;

public abstract class BaseService {

	protected Logger log = null;
	
	public abstract void createLogger();
	
	public ClienteServiceException registrarErro(Exception e) {
		log.error("Erro ao executar a ação");
		log.error(e,e);
		return new ClienteServiceException(e);
	}
}
