package br.com.platform.builders.modelo;

import java.io.Serializable;

public class Erro implements Serializable {


	private static final long serialVersionUID = 1L;
	private int codigo;
	private String descricao;
	private String mensagemErro;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getMensagemErro() {
		return mensagemErro;
	}
	public void setMensagemErro(String mensagemErro) {
		this.mensagemErro = mensagemErro;
	}
	
	
	
}
