package br.com.platform.builders.modelo;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.platform.builders.annotations.CPF;
import br.com.platform.builders.annotations.Data;
import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@Schema(description = "Serviços CRUD do objeto Cliente")
public class Cliente implements Serializable {
	

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	private Long id;
	@Schema(name = "nome", example = "Tobias Liberato Vilaverde", type = "String", description = "Nome do cliente")
	@NotNull(message = "O campo nome não pode ser nulo.")
	private String nome;
	@CPF(true)
	@Schema(name = "cpf", example = "54986102036", type = "String", description = "CPF do cliente")
	private String cpf;
	@NotNull(message = "O campo dataNascimento não pode ser nulo.")
	@PastOrPresent(message = "A data não pode ser futura, ou está no formato errado")
	@Data(pattern = "dd/MM/yyyy", message = "Formato de data inválida")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Schema(name = "dataNascimento", type = "Date", example = "17/05/1982", description = "Data do nascimento do cliente")
	private Date dataNascimento;
	
	public Cliente() {
	
	}
	
	
	public Cliente( String nome, String cpf, Date dataNascimento) {
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@JsonGetter("idade")
	public int getIdade() {
		LocalDate dtNascimento = Instant.ofEpochMilli(dataNascimento.getTime())
								      .atZone(ZoneId.of("America/Sao_Paulo"))
								      .toLocalDate();
		return Period.between(dtNascimento,LocalDate.now()).getYears();
	}

}
