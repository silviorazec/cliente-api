package br.com.platform.builders.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.platform.builders.modelo.Cliente;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long> {

  Page<Cliente> findAllByNomeContainingIgnoreCaseAndCpfContaining(String nome, String cpf, Pageable pageable);
  
  Cliente findByCpf(String cpf);
  
}