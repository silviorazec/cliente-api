package br.com.platform.builders.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.platform.builders.annotations.Data;

public class DataValidator implements ConstraintValidator<Data, Date> {

	private String pattern;
	
	@Override
	public void initialize(Data constraintAnnotation) {
	
		pattern = constraintAnnotation.pattern();
	}

	@Override
	public boolean isValid(Date value, ConstraintValidatorContext context) {
		try {
			new SimpleDateFormat(pattern).format(value);
			return true;
		}catch (Exception e) {
			return false;
		}
	}



}
