package br.com.platform.builders.utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import br.com.platform.builders.annotations.CPF;

public class CPFValidator implements ConstraintValidator<CPF, String> {


	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return StringUtils.isEmpty(value) ? false : UtilsValidate.isCPF(value);
	}

}
