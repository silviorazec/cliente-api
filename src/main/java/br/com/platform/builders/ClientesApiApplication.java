package br.com.platform.builders;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.platform.builders.modelo.Cliente;
import br.com.platform.builders.repositories.ClienteRepository;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class ClientesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientesApiApplication.class, args);
	}

	


	 @Bean
	  public OpenAPI customOpenAPI(@Value("${application-description}") String appDesciption, @Value("${application-version}") String appVersion) {

	   return new OpenAPI()
	        .info(new Info()
	        .title("Rest API de Cliente")
	        .version(appVersion)
	        .description(appDesciption)
	        .termsOfService("http://swagger.io/terms/"));
	 }

	    
	
    @Bean
    public CommandLineRunner init(ClienteRepository repository) {
        return args -> {	
            repository.deleteAll();
            repository.save(new Cliente("Yaroslav Dinis Afonso","54986102036", new SimpleDateFormat("dd/MM/yyyy").parse("17/05/1982")));
            repository.save(new Cliente("Alex Rebouças Rebelo","90552231070", new SimpleDateFormat("dd/MM/yyyy").parse("09/07/1980")));
            repository.save(new Cliente("Tobias Liberato Vilaverde","47567375010", new SimpleDateFormat("dd/MM/yyyy").parse("21/11/1984")));
            repository.save(new Cliente("Isabelly Modesto Cadavez","47567375010", new SimpleDateFormat("dd/MM/yyyy").parse("07/09/1990")));
            repository.save(new Cliente("Vítor Belmonte Vila-Chã","70757814050", new SimpleDateFormat("dd/MM/yyyy").parse("22/04/1997")));
            repository.save(new Cliente("Lidiana Grilo Peralta","78863897000", new SimpleDateFormat("dd/MM/yyyy").parse("27/06/1990")));
            repository.save(new Cliente("Nuna Anjos Rufino","59724281051", new SimpleDateFormat("dd/MM/yyyy").parse("09/05/1985")));
            repository.save(new Cliente("Catalina Bivar Saraiva","72749327059", new SimpleDateFormat("dd/MM/yyyy").parse("11/10/1982")));
            repository.save(new Cliente("Lya Câmara Casqueira","99996941019", new SimpleDateFormat("dd/MM/yyyy").parse("05/12/1988")));
            repository.save(new Cliente("Hossana Miranda Conde","16599234097", new SimpleDateFormat("dd/MM/yyyy").parse("20/03/1999")));
            repository.save(new Cliente("Isaque Fernandes Valgueiro","63798662053", new SimpleDateFormat("dd/MM/yyyy").parse("18/02/1970")));
            repository.save(new Cliente("Samuel Candeias Rocha","12648051015", new SimpleDateFormat("dd/MM/yyyy").parse("02/05/1991")));
            repository.save(new Cliente("Kendrick Talhão Caparica","27150245099", new SimpleDateFormat("dd/MM/yyyy").parse("30/08/1998")));
            repository.save(new Cliente("Indira Amaral Pimenta","76928167009", new SimpleDateFormat("dd/MM/yyyy").parse("07/11/2000")));
            repository.save(new Cliente("Ianis Cascais Dias","02394188081", new SimpleDateFormat("dd/MM/yyyy").parse("15/12/2005")));
            repository.save(new Cliente("Luz Abelho Taveiros","91684853010", new SimpleDateFormat("dd/MM/yyyy").parse("09/01/1984")));
            repository.save(new Cliente("Kelly Onofre Rolim","37140103003", new SimpleDateFormat("dd/MM/yyyy").parse("11/04/1982")));
            repository.save(new Cliente("Aaliyah Serro Caldeira","74853795006", new SimpleDateFormat("dd/MM/yyyy").parse("12/08/1990")));
            repository.save(new Cliente("Stacy Pescada Fonseca","65352536009", new SimpleDateFormat("dd/MM/yyyy").parse("01/03/1986")));
        };
    }
    
   
}
