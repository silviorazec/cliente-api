#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn install -DskipTests -f /home/app/pom.xml clean package

#
# Package stage
#
FROM openjdk:8-jdk-alpine
RUN apk add --no-cache bash
COPY --from=build /home/app/target/clientes-api-0.0.1-SNAPSHOT.jar /usr/local/lib/clientes-api-0.0.1-SNAPSHOT.jar
COPY wait-for-it.sh /wait-for-it.sh
RUN chmod +x /wait-for-it.sh
EXPOSE 8080
